package com.s2d5.hw03myidol;

/**
 * Created by gilbert-mac on 2017. 7. 3..
 */

public class MyGroup {
    int Image;
    String name;

    public MyGroup(int image, String name) {
        Image = image;
        this.name = name;
    }

    public int getImage() {
        return Image;
    }

    public void setImage(int image) {
        Image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
