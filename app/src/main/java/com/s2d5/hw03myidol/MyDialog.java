package com.s2d5.hw03myidol;


import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by gilbert-mac on 2017. 7. 3..
 */

public class MyDialog extends Dialog {
    ImageView imageView;
    TextView title;
    TextView description;


    public MyDialog(@NonNull Context context, MyMember data) {
        super(context);
        setContentView(R.layout.dialog);

        imageView = (ImageView)findViewById(R.id.dialog_image);
        title = (TextView)findViewById(R.id.dialog_title);
        description = (TextView) findViewById(R.id.dialog_description);

        imageView.setImageResource(data.getImage());
        title.setText(data.getName());
        description.setText(data.getDescription());
    }
}
