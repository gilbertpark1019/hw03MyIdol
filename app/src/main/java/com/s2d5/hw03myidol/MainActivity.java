package com.s2d5.hw03myidol;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    LinearLayout root;
    ArrayList<MyGroup> Group = new ArrayList<>();

    AppCompatSpinner spinner;
    ArrayAdapter<String> spinnerAdapter;

    ListView listview;
    MyAdapter adapter;

    int groupId = -1;
    MyDialog dialog;

    ArrayList<MyMember> Apink = new ArrayList<>();
    ArrayList<MyMember> BTS = new ArrayList<>();
    ArrayList<MyMember> EXO = new ArrayList<>();
    ArrayList<MyMember> twice = new ArrayList<>();
    ArrayList<MyMember> RedVelvet = new ArrayList<>();

    ArrayList<String> strApink = new ArrayList<>();
    ArrayList<String> strBTS = new ArrayList<>();
    ArrayList<String> strEXO = new ArrayList<>();
    ArrayList<String> strTwice = new ArrayList<>();
    ArrayList<String> strRedvelvet = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        root = (LinearLayout) findViewById(R.id.root);
        spinner = (AppCompatSpinner) findViewById(R.id.spinner);
        listview = (ListView) findViewById(R.id.listview);
        spinner.setOnItemSelectedListener(this);

        SetGroup();
        SetApink();
        SetBTS();
        SetEXO();
        SetTwice();
        SetRedVelvet();
    }

    public void SetSpinner(int position) {
        groupId = position;
        switch (position) {
            case 0:
                root.setBackgroundResource(R.mipmap.apink);
                spinnerAdapter = new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_item, strApink);
                spinner.setAdapter(spinnerAdapter);
                break;
            case 1:
                root.setBackgroundResource(R.mipmap.bts);
                spinnerAdapter = new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_item, strBTS);
                spinner.setAdapter(spinnerAdapter);
                break;
            case 2:
                root.setBackgroundResource(R.mipmap.exo);
                spinnerAdapter = new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_item, strEXO);
                spinner.setAdapter(spinnerAdapter);
                break;
            case 3:
                root.setBackgroundResource(R.mipmap.twice);
                spinnerAdapter = new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_item, strTwice);
                spinner.setAdapter(spinnerAdapter);
                break;
            case 4:
                root.setBackgroundResource(R.mipmap.redvelvet);
                spinnerAdapter = new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_item, strRedvelvet);
                spinner.setAdapter(spinnerAdapter);
                break;
            default:
                break;

        }
    }

    private void SetGroup() {
        String[] groupArray = getResources().getStringArray(R.array.group);
        Group.add(new MyGroup(R.mipmap.apink, groupArray[0]));
        Group.add(new MyGroup(R.mipmap.bts, groupArray[1]));
        Group.add(new MyGroup(R.mipmap.exo, groupArray[2]));
        Group.add(new MyGroup(R.mipmap.twice, groupArray[3]));
        Group.add(new MyGroup(R.mipmap.redvelvet, groupArray[4]));

        adapter = new MyAdapter(MainActivity.this, Group);
        listview.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void SetRedVelvet() {
        String[] redvelvetArray = getResources().getStringArray(R.array.redvelvet);
        for (String str : redvelvetArray) {
            strRedvelvet.add(str.split("@")[0]);
        }

        RedVelvet.add(new MyMember(R.mipmap.redvelvet_01_irene, redvelvetArray[0].split("@")[0], redvelvetArray[0].split("@")[1]));
        RedVelvet.add(new MyMember(R.mipmap.redvelvet_02_seulgi, redvelvetArray[1].split("@")[0], redvelvetArray[1].split("@")[1]));
        RedVelvet.add(new MyMember(R.mipmap.redvelvet_03_wendy, redvelvetArray[2].split("@")[0], redvelvetArray[2].split("@")[1]));
        RedVelvet.add(new MyMember(R.mipmap.redvelvet_04_joy, redvelvetArray[3].split("@")[0], redvelvetArray[3].split("@")[1]));
        RedVelvet.add(new MyMember(R.mipmap.redvelvet_05_yeri, redvelvetArray[4].split("@")[0], redvelvetArray[4].split("@")[1]));
    }

    private void SetTwice() {
        String[] twiceArray = getResources().getStringArray(R.array.twice);
        for (String str : twiceArray) {
            strTwice.add(str.split("@")[0]);
        }

        twice.add(new MyMember(R.mipmap.twice_01nayeon, twiceArray[0].split("@")[0], twiceArray[0].split("@")[1]));
        twice.add(new MyMember(R.mipmap.twice_02jeongyeon, twiceArray[1].split("@")[0], twiceArray[1].split("@")[1]));
        twice.add(new MyMember(R.mipmap.twice_03_jihyo, twiceArray[2].split("@")[0], twiceArray[2].split("@")[1]));
        twice.add(new MyMember(R.mipmap.twice_04mina, twiceArray[3].split("@")[0], twiceArray[3].split("@")[1]));
        twice.add(new MyMember(R.mipmap.twice_05tzuyu, twiceArray[4].split("@")[0], twiceArray[4].split("@")[1]));
        twice.add(new MyMember(R.mipmap.twice_06sana, twiceArray[5].split("@")[0], twiceArray[5].split("@")[1]));
        twice.add(new MyMember(R.mipmap.twice_07dahyun, twiceArray[6].split("@")[0], twiceArray[6].split("@")[1]));
        twice.add(new MyMember(R.mipmap.twice_08chaeyeong, twiceArray[7].split("@")[0], twiceArray[7].split("@")[1]));
        twice.add(new MyMember(R.mipmap.twice_09momo, twiceArray[8].split("@")[0], twiceArray[8].split("@")[1]));
    }

    private void SetEXO() {
        String[] exoArray = getResources().getStringArray(R.array.EXO);
        for (String str : exoArray) {
            strEXO.add(str.split("@")[0]);
        }

        EXO.add(new MyMember(R.mipmap.exo_01suho, exoArray[0].split("@")[0], exoArray[0].split("@")[1]));
        EXO.add(new MyMember(R.mipmap.exo_02baekhyeon, exoArray[1].split("@")[0], exoArray[1].split("@")[1]));
        EXO.add(new MyMember(R.mipmap.exo_03chanyeol, exoArray[2].split("@")[0], exoArray[2].split("@")[1]));
        EXO.add(new MyMember(R.mipmap.exo_04dio, exoArray[3].split("@")[0], exoArray[3].split("@")[1]));
        EXO.add(new MyMember(R.mipmap.exo_05kai, exoArray[4].split("@")[0], exoArray[4].split("@")[1]));
        EXO.add(new MyMember(R.mipmap.exo_06sehun, exoArray[5].split("@")[0], exoArray[5].split("@")[1]));
        EXO.add(new MyMember(R.mipmap.exo_07xiumin, exoArray[6].split("@")[0], exoArray[6].split("@")[1]));
        EXO.add(new MyMember(R.mipmap.exo_08lay, exoArray[7].split("@")[0], exoArray[7].split("@")[1]));
        EXO.add(new MyMember(R.mipmap.exo_09chen, exoArray[8].split("@")[0], exoArray[8].split("@")[1]));
    }

    private void SetBTS() {
        String[] btsArray = getResources().getStringArray(R.array.BTS);
        for (String str : btsArray) {
            strBTS.add(str.split("@")[0]);
        }

        BTS.add(new MyMember(R.mipmap.bts_01jhope, btsArray[0].split("@")[0], btsArray[0].split("@")[1]));
        BTS.add(new MyMember(R.mipmap.bts_02jimin, btsArray[1].split("@")[0], btsArray[1].split("@")[1]));
        BTS.add(new MyMember(R.mipmap.bts_03jin, btsArray[2].split("@")[0], btsArray[2].split("@")[1]));
        BTS.add(new MyMember(R.mipmap.bts_04jungguk, btsArray[3].split("@")[0], btsArray[3].split("@")[1]));
        BTS.add(new MyMember(R.mipmap.bts_05rapmonster, btsArray[4].split("@")[0], btsArray[4].split("@")[1]));
        BTS.add(new MyMember(R.mipmap.bts_06suga, btsArray[5].split("@")[0], btsArray[5].split("@")[1]));
        BTS.add(new MyMember(R.mipmap.bts_07v, btsArray[6].split("@")[0], btsArray[6].split("@")[1]));

    }

    private void SetApink() {
        String[] apinkArray = getResources().getStringArray(R.array.Apink);
        for (String str : apinkArray) {
            strApink.add(str.split("@")[0]);
        }

        Apink.add(new MyMember(R.mipmap.apink_01chorong, apinkArray[0].split("@")[0], apinkArray[0].split("@")[1]));
        Apink.add(new MyMember(R.mipmap.apink_02bomi, apinkArray[1].split("@")[0], apinkArray[1].split("@")[1]));
        Apink.add(new MyMember(R.mipmap.apink_03eunji, apinkArray[2].split("@")[0], apinkArray[2].split("@")[1]));
        Apink.add(new MyMember(R.mipmap.apink_04naeun, apinkArray[3].split("@")[0], apinkArray[3].split("@")[1]));
        Apink.add(new MyMember(R.mipmap.apink_05namju, apinkArray[4].split("@")[0], apinkArray[4].split("@")[1]));
        Apink.add(new MyMember(R.mipmap.apink_06hayoung, apinkArray[5].split("@")[0], apinkArray[5].split("@")[1]));
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (groupId) {
            case 0:
                dialog = new MyDialog(MainActivity.this, Apink.get(position));
                dialog.show();
                break;
            case 1:
                dialog = new MyDialog(MainActivity.this, BTS.get(position));
                dialog.show();
                break;
            case 2:
                dialog = new MyDialog(MainActivity.this, EXO.get(position));
                dialog.show();
                break;
            case 3:
                dialog = new MyDialog(MainActivity.this, twice.get(position));
                dialog.show();
                break;
            case 4:
                dialog = new MyDialog(MainActivity.this, RedVelvet.get(position));
                dialog.show();
                break;
            default:
                break;

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
