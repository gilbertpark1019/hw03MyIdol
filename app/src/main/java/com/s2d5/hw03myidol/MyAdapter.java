package com.s2d5.hw03myidol;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by gilbert-mac on 2017. 7. 1..
 */

public class MyAdapter extends BaseAdapter {
    Context context;
    ArrayList<MyGroup> datas;

    public MyAdapter(Context context, ArrayList<MyGroup> datas) {
        this.context = context;
        this.datas = datas;
    }

    @Override
    public int getCount() {
        return datas.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Holder holder;
        if(convertView==null) {
            LayoutInflater mInflater =
                    (LayoutInflater)context.getSystemService
                            (Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.group_item, parent, false);
            holder = new Holder();
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.image =(ImageView)convertView.findViewById(R.id.group_image);
        holder.name =(TextView)convertView.findViewById(R.id.group_name);

        holder.image.setImageResource(datas.get(position).getImage());
        holder.name.setText(datas.get(position).getName());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)context).SetSpinner(position);
            }
        });

        return convertView;
    }

    public class Holder{
        public ImageView image;
        public TextView name;
    }
}
